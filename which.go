package main

import (
	"fmt"
	"os"
	"regexp"
	"strings"

	"github.com/docopt/docopt-go"
)

type Finder interface {
	Which() ([]string, error)
	WhichOne() (string, error)
}

type finder struct {
	pattern *regexp.Regexp
	exts    []string
	paths   []string
}

func (f *finder) findInDir(dir string) ([]string, error) {
	return PathFinder(f.pattern, dir, f.exts)
}

func (f *finder) Which() ([]string, error) {
	var s []string
	for _, dir := range f.paths {
		results, _ := f.findInDir(dir)
		for _, result := range results {
			s = append(s, result)
		}
	}
	return s, nil
}
func (f *finder) WhichOne() (string, error) {
	for _, dir := range f.paths {
		results, _ := f.findInDir(dir)
		if len(results) > 0 {
			return results[0], nil
		}
	}
	return "", fmt.Errorf("that was bad")
}

func NewFinder(pattern string, exts []string, paths []string) Finder {
	pattern = regexp.QuoteMeta(pattern)
	pattern = strings.ReplaceAll(pattern, "\\*", ".*?")
	pattern = strings.ReplaceAll(pattern, "\\?", ".")
	pattern = "^(?i)" + pattern + "$"
	p, _ := regexp.Compile(pattern)
	f := finder{pattern: p, exts: exts, paths: paths}
	return &f
}

func main() {
	usage := `which

Usage:
    which <name>
    which -a <name>
    which -h | --help

Options:
    -h --help    Show this screen.
    -a --all     Show all executables matching the pattern.`
	arguments, _ := docopt.ParseDoc(usage)

	var name string
	name, _ = arguments.String("<name>")

	var paths []string
	var exts []string
	cwd, _ := os.Getwd()
	paths = append(paths, cwd)

	if pathEnv, ok := os.LookupEnv("PATH"); ok {
		for _, p := range strings.Split(pathEnv, string(os.PathListSeparator)) {
			paths = append(paths, p)
		}
	} else {
		fmt.Println("Failed to read PATH")
		os.Exit(1)
	}
	if pathextEnv, ok := os.LookupEnv("PATHEXT"); ok {
		exts = strings.Split(pathextEnv, string(os.PathListSeparator))
	}
	f := NewFinder(name, exts, paths)
	var findAll bool = false
	findAll, _ = arguments.Bool("--all")
	var names []string
	var err error
	if findAll {
		names, err = f.Which()
	} else {
		var name string
		name, err = f.WhichOne()
		names = append(names, name)
	}
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}
	for _, name := range names {
		fmt.Println(name)
	}
	os.Exit(0)
}
