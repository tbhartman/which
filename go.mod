module gitlab.com/tbhartman/which

go 1.13

require (
	github.com/docopt/docopt-go v0.0.0-20180111231733-ee0de3bc6815
	golang.org/x/lint v0.0.0-20200130185559-910be7a94367 // indirect
)
