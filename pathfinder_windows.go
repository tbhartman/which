package main

import (
	"io/ioutil"
	"path/filepath"
	"regexp"
	"sort"
	"strings"
)

type nameElement struct {
	toCompare string
	ext       string
	full      string
}

// byExt implements sort.Interface for paths using ext
type byExt struct {
	s    []nameElement
	exts map[string]int
}

func (b byExt) Len() int      { return len(b.s) }
func (b byExt) Swap(i, j int) { b.s[i], b.s[j] = b.s[j], b.s[i] }
func (b byExt) Less(i, j int) bool {
	if strings.Compare(b.s[i].ext, b.s[j].ext) != 0 {
		iExtValue, iOk := b.exts[b.s[i].ext]
		jExtValue, jOk := b.exts[b.s[j].ext]
		if iOk && !jOk {
			return false
		} else if !iOk && jOk {
			return true
		} else if iOk && jOk {
			return iExtValue < jExtValue
		} else {
			return strings.Compare(b.s[i].full, b.s[j].full) < 0
		}
	} else {
		return strings.Compare(b.s[i].full, b.s[j].full) < 0
	}
}

func PathFinder(pattern *regexp.Regexp, dir string, userExts []string) ([]string, error) {
	patternString := "(?i)" + pattern.String()
	pattern, _ = regexp.Compile(patternString)
	var exts []string
	if len(userExts) == 0 {
		// https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/start#remarks
		exts = []string{".COM", ".EXE", ".BAT", ".CMD", ".VBS", ".VBE",
			".JS", ".JSE", ".WSF", ".WSH", ".MSC"}
	} else {
		for _, ext := range userExts {
			exts = append(exts, strings.ToUpper(ext))
		}
	}
	extsMap := make(map[string]int)
	for i, ext := range exts {
		extsMap[ext] = i
	}

	fileinfos, _ := ioutil.ReadDir(dir)
	var filenames []nameElement
	for _, f := range fileinfos {
		if f.IsDir() {
			continue
		}
		e := nameElement{
			full:      f.Name(),
			ext:       strings.ToUpper(filepath.Ext(f.Name())),
			toCompare: strings.ToUpper(f.Name())}
		for _, ext := range exts {
			if strings.Compare(e.ext, ext) == 0 {
				e.toCompare = strings.ToUpper(e.full[0 : len(e.full)-len(ext)])
				continue
			}
		}
		filenames = append(filenames, e)
	}
	b := byExt{s: filenames, exts: extsMap}
	sort.Sort(b)
	var s []string
	for _, filename := range b.s {
		if pattern.MatchString(filename.toCompare) {
			s = append(s, filepath.Join(dir, filename.full))
		} else if pattern.MatchString(strings.ToUpper(filename.full)) {
			s = append(s, filepath.Join(dir, filename.full))
		}
	}
	return s, nil
}
